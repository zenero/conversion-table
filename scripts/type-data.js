let dataSet = {
   mass: {
      microgram: 1000000000,
      miligram: 1000000,
      gram: 1000,
      kilogram: 1, //Default value
      tonne: 0.001,
   },
   length: {
      nanometre: 10000000,
      micrometre: 10000,
      millimetre: 10,
      centimetre: 1, //Default value
      yard: 0.0109361,
      metre: 0.01,
      kilometre: 0.000001,
      mile: 0.0000062137,
   },
   temperature: {
      celsius: 1, //Default value
      fahrenheit: {
         offset: -32,
         ratio:  1.8,
      },
      kelvin: {
         offset: -273.15,
         ratio: 1,
      },
   },
   speed: {
      mph: 1, //Default value
      kph: 1.60934,
      mps: 0.44704,
      fps: 1.46667,
      knot: 0.868976,
   },
   area: {
      "square kilometre": 1, //Default value
      "square metre": 1000000,
      "square mile": 0.386102,
      "square yard": 1196000,
      "square foot": 10760000,
      "square inch": 1550000000,
      hectare: 100,
      acre: 247.105,
   }
}