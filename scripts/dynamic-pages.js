function changeMainContent() {
    
}

function localisedGreetings() {
    let date = new Date()
    let now = date.getHours();
    let greeting = getElement("greeting")

    if (now >= 18) {
        greeting.innerHTML += " evening!";
    } else if (now > 12) {
        greeting.innerHTML += " afternoon!";
    } else {
        greeting.innerHTML += " morning!";
    }
}