/**
 * @summary Gets an HTML element from Id OR Class. 
 * @param {String} idOrClass name of Id or Class
 * @return {HTMLElement}  
 */  
function getElement(idOrClass) {

    let element = document.getElementById(idOrClass)
    if (!element) {
        element = document.getElementsByClassName(idOrClass)
    }
    return element
}

/**
 * @summary Gets a value from an input using getElement to get id. 
 * @param {String} id name of Id
 * @return {value} the value in the input selected
 */ 
function getValueFromInput(id) {

    return getElement(id).value
}

/**
 * @summary Change the text of an elements inner HTML.
 * @param {String} idOrClass The Id or Class name on desired element.
 * @param {String} text The text to add to your element.
 */
function changeElementText(idOrClass,text) {

    getElement(idOrClass).innerHTML = text
}

