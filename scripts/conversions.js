/**
 * @summary takes the selected options from the drop downs to convert from one unit of measure to the next.
 */
function convert() {
    let typeOfUnits = getElement("typeDropDown").value

    let selectedUnits = getElement("fromDropDown").value
    let desiredUnits = getElement("toDropDown").value

    let valueToConvert = getValueFromInput("input") * 1
    
    let from = dataSet[typeOfUnits][selectedUnits]
    let to = dataSet[typeOfUnits][desiredUnits]

    let fromOffset = from.offset || 0
    let toOffset = to.offset || 0

    let fromRatio = from.ratio || from
    let toRatio = to.ratio || to

    let result = ((valueToConvert + fromOffset)/ fromRatio) * toRatio - toOffset

    changeElementText("result", result)
}

function areDropDownsDifferent(){
    let selectedUnits = getElement("fromDropDown").value
    let desiredUnits = getElement("toDropDown").value

    if (selectedUnits == desiredUnits) {
        makeSelectedOptionsDifferent()
    }
}

function makeSelectedOptionsDifferent(){
    let selectedUnits = getElement("fromDropDown").value
    let desiredUnits = getElement("toDropDown").value


    
}

/**
 * @summary Dropdown for initial category of measure selected,
 *  this will populated the dropdowns for the units of measurement 
 *  in the selected category.
 */
function typeOfMeasurementSelected() {
    let typeDropDown = getElement("typeDropDown").value
    populateSelects(dataSet[typeDropDown])
}

function populateSelects(dataObject) {
    let fromDropDown = getElement("fromDropDown")
    let toDropDown = getElement("toDropDown")
    
    fromDropDown.innerHTML = "";
    toDropDown.innerHTML = "";

    addOptionsToASelect(fromDropDown,dataObject)
    addOptionsToASelect(toDropDown,dataObject)
}

function addOptionsToASelect(dropDown,dataObject) { 
    for (let i = 0; i < Object.keys(dataObject).length; i++){
        var newlyCreatedOption = document.createElement("option")
        newlyCreatedOption.text = Object.keys(dataObject)[i]
        dropDown.add(newlyCreatedOption)
    }
}

