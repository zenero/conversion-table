Conversion project

### Rules

## Folder Structure

HTML, CSS, Js are split into their own folders of structure, style, and scripts respectively.  
I have also included a resource folder which will hold any files that may be included, such as images, audio, video etc.

## CSS

I'm attempting to follow the BEM methodology, this can be found here:  
[BEM guidelines](http://getbem.com/)

## Js

Plain Javascript is intended. I may include other tools, frameworks or libraries if necessary.  